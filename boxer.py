from __future__ import division

from math import ceil
import numpy as np
from matplotlib import pyplot as plt
import cv2
import pandas as pd 
from keras.optimizers import Adam
from keras.engine.topology import InputSpec
from keras.engine.topology import Layer
from keras.layers import Input, Lambda, Conv2D, MaxPooling2D, BatchNormalization, ELU, Reshape, Concatenate, Activation
from keras.regularizers import l2
from keras.models import Model
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau, TerminateOnNaN, CSVLogger
from keras import backend as K
from keras.models import load_model

from models.keras_ssd300 import ssd_300
from models.keras_ssd7 import build_model
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_L2Normalization import L2Normalization
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast

from ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

from data_generator.object_detection_2d_data_generator import DataGenerator
from data_generator.object_detection_2d_misc_utils import apply_inverse_transforms
from data_generator.data_augmentation_chain_variable_input_size import DataAugmentationVariableInputSize
from data_generator.data_augmentation_chain_constant_input_size import DataAugmentationConstantInputSize
from data_generator.data_augmentation_chain_original_ssd import SSDDataAugmentation
from bounding_box_utils.bounding_box_utils import convert_coordinates
from data_generator.object_detection_2d_geometric_ops import Resize, RandomFlip


import numpy as np
import keras.backend as K
from keras.engine.topology import InputSpec
from keras.engine.topology import Layer

from bounding_box_utils.bounding_box_utils import convert_coordinates

from keras.layers import Input, Lambda, Conv2D, MaxPooling2D, BatchNormalization, ELU, Reshape, Concatenate, Activation
from keras.regularizers import l2
# from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras.models import Model

from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_L2Normalization import L2Normalization
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast

K.clear_session() # Clear previous models from memory.

mean_color = [128, 128, 128]
swap_channels = [2, 1, 0]
img_height = 32*5 # Height of the input images
img_width = 32*25 # Width of the input images
img_channels = 3 # Number of color channels of the input images
intensity_mean = 127.5 # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
intensity_range = 127.5 # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
n_classes = 11 # Number of positive classes
scales = [0.08, 0.16, 0.32, 0.64, 0.96] # An explicit list of anchor box scaling factors. If this is passed, it will override `min_scale` and `max_scale`.
aspect_ratios = [0.5, 1.0, 2.0] # The list of aspect ratios for the anchor boxes
two_boxes_for_ar1 = True # Whether or not you want to generate two anchor boxes for aspect ratio 1
steps = None # In case you'd like to set the step sizes for the anchor box grids manually; not recommended
offsets = None # In case you'd like to set the offsets for the anchor box grids manually; not recommended
clip_boxes = False # Whether or not to clip the anchor boxes to lie entirely within the image boundaries
variances = [1.0, 1.0, 1.0, 1.0] # The list of variances by which the encoded target coordinates are scaled
normalize_coords = True # Whether or

adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
# Set the batch size.
batch_size = 16

# 4: Define the image processing chain.
data_augmentation_chain = DataAugmentationVariableInputSize(random_brightness=(-48, 48, 0.5),
															random_contrast=(0.5, 1.8, 0.5),
															random_saturation=(0.5, 1.8, 0.5),
															random_hue=(18, 0.5),
															random_flip=0,
															background=(0,0,0),
															resize_height= img_height,
															resize_width=img_width)


# 6: Create the generator handles that will be passed to Keras' `fit_generator()` function.
from data_generator.object_detection_2d_geometric_ops import Resize, RandomFlip
from scipy.ndimage import label as label_from_scipy

train_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None)
val_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None)

# 2: Parse the image and label lists for the training and validation datasets.
# TODO: Set the paths to your dataset here.
# Images

images_dir = ''

train_dataset.parse_csv(images_dir=images_dir,
						labels_filename='data/train.csv',
						input_format=['image_name', 'xmin', 'xmax', 'ymin', 'ymax', 'class_id'], # This is the order of the first six columns in the CSV file that contains the labels for your dataset. If your labels are in XML format, maybe the XML parser will be helpful, check the documentation.
						include_classes='all')

val_dataset.parse_csv(images_dir=images_dir,
					  labels_filename='data/test.csv',
					  input_format=['image_name', 'xmin', 'xmax', 'ymin', 'ymax', 'class_id'],
					  include_classes='all')

class Boxer():
	def __init__(self,model_dir,mode='test'):
		
		self.model = build_model(image_size=(img_height, img_width, img_channels),
					n_classes=n_classes,
					mode='training',
					l2_regularization=0.0005,
					scales=scales,
					aspect_ratios_global=aspect_ratios,
					aspect_ratios_per_layer=None,
					two_boxes_for_ar1=two_boxes_for_ar1,
					steps=steps,
					offsets=offsets,
					clip_boxes=clip_boxes,
					variances=variances,
					normalize_coords=normalize_coords,
					subtract_mean=intensity_mean,
					divide_by_stddev=intensity_range)
	
		if mode =='train':
			self.model.compile(optimizer=adam, loss=ssd_loss.compute_loss)
			self.predictor_sizes = [self.model.get_layer('classes4').output_shape[1:3],
			self.model.get_layer('classes5').output_shape[1:3],
			self.model.get_layer('classes6').output_shape[1:3],
			self.model.get_layer('classes7').output_shape[1:3]]
		   # 5: Instantiate an encoder that can encode ground truth labels into the format needed by the SSD loss function.
		   # The encoder constructor needs the spatial dimensions of the model's predictor layers to create the anchor boxes.

			self.ssd_input_encoder = SSDInputEncoder(img_height=img_height,
								img_width=img_width,
								n_classes=n_classes,
								predictor_sizes=self.predictor_sizes,
								scales=scales,
								aspect_ratios_global=aspect_ratios,
								two_boxes_for_ar1=two_boxes_for_ar1,
								steps=steps,
								offsets=offsets,
								clip_boxes=clip_boxes,
								variances=variances,
								matching_type='multi',
								pos_iou_threshold=0.5,
								neg_iou_limit=0.3,
								normalize_coords=normalize_coords)
		else:
			self.model.load_weights(model_dir+'/ocr_ssd7.h5', by_name=True)
	
	def train(save_path = ''):		
		train_generator = train_dataset.generate(batch_size=batch_size,
												 shuffle=True,
												 transformations=[DataAugmentationVariableInputSize(img_height,img_width)],
												 label_encoder=self.ssd_input_encoder,
												 returns={'processed_images',
														  'encoded_labels'},
												 keep_images_without_gt=False)

		val_generator = val_dataset.generate(batch_size=batch_size,
											 shuffle=False,
											 transformations=[Resize(img_height,img_width)],
											 label_encoder=self.ssd_input_encoder,
											 returns={'processed_images',
													  'encoded_labels'},
											 keep_images_without_gt=False)

		model_checkpoint = ModelCheckpoint(filepath=save_path+'ssd7x_epoch-{epoch:02d}_loss-{loss:.4f}_val_loss-{val_loss:.4f}.h5',
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=True,
                                   save_weights_only=False,
                                   mode='auto',
                                   period=10)

		csv_logger = CSVLogger(filename=save_path+'ssd7x_training_log.csv',
                       separator=',',
                       append=True)

		early_stopping = EarlyStopping(monitor='val_loss',
                               min_delta=0.0,
                               patience=10,
                               verbose=1)

		reduce_learning_rate = ReduceLROnPlateau(monitor='val_loss',
                                         factor=0.2,
                                         patience=4,
                                         verbose=1,
                                         epsilon=0.01,
                                         cooldown=0,
                                         min_lr=3e-4)

		callbacks = [model_checkpoint, csv_logger, early_stopping, reduce_learning_rate]

		initial_epoch   = 0
		final_epoch     = 40
		steps_per_epoch = 256

		history = self.model.fit_generator(generator=train_generator,
                              steps_per_epoch=steps_per_epoch,
                              epochs=final_epoch,
                              callbacks=callbacks,
                              validation_data=val_generator,
                              validation_steps=ceil(val_dataset_size/batch_size),
                              initial_epoch=0)

	def remove_overlapping(self,y_pred_decoded):
		df = pd.DataFrame(y_pred_decoded[0]).sort_values(2).reset_index(drop=True)
		df[0] = df[0]-1
		med = df[2].diff().median()
		mask = df[2].diff() > (med-med*0.4)
		mask[0] =True
		return df.groupby(mask.cumsum()).apply(lambda x : x.loc[x[1].idxmax()])[0].values.astype(int).astype(str)
	
	def get_boxes(self,img):
		img = Resize(img_height,img_width)(img)
		y_pred = self.model.predict(np.array([img]))
		y_pred_decoded = decode_detections(y_pred,
								   confidence_thresh=0.2,
								   iou_threshold=0.1,
								   top_k=20,
								   normalize_coords=normalize_coords,
								   img_height=img_height,
								   img_width=img_width)
		return self.remove_overlapping(y_pred_decoded)
