from __future__ import division

import os
import pickle
from io import BytesIO
import numpy as np
from tensorflow import keras
from PIL import Image
from base64 import b64decode
from boxer import Boxer
import sys
import cv2

np.set_printoptions(precision=2, suppress=True, linewidth=90)

class Predictor(object):

    def __init__(self, model):
        self.model = model
        
    def predict(self, path, **kwargs):
        img = cv2.imread(path)
        return  self.model.get_boxes(img)

    @classmethod
    def from_path(cls, model_dir):
        """Creates an instance of Predictor using the given path.

        This loads artifacts that have been copied from model directory 
        Predictor uses them during prediction.

        Args:
            model_dir: The local directory that contains the trained Keras
                model
        Returns:    
            An instance of `MyPredictor`.
        """
        model = Boxer(model_dir)
    
        return cls(model)

def main():
    
    img_path = sys.argv[1]
        
    op = Predictor.from_path('model_files')
    preds = op.predict(img_path)
    # preds = preds[preds[:,2].argsort()]
    print('K Number:', ''.join(preds))

if __name__== "__main__":
    main()