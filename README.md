Usage: 

1. Clone the repository 
2. Prerequisites: 

 - 'opencv-python', 
 - 'Pillow',  
 - 'scikit-image', 
 - 'tqdm',
 - 'keras==2.2.4'

3. Command line example :
```
python3 predictor.py example_input/1.png
```


