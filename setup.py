from setuptools import setup

setup(
    name='main_code',
    version='0.1',
    scripts=['boxer.py', 'predictor.py'],    
    packages=['misc_utils', 'bounding_box_utils', 'keras_layers', 'models',  'keras_loss_function', 'ssd_encoder_decoder', 'eval_utils','data_generator'],
    install_requires=[ 'opencv-python', 'Pillow', 'scikit-image', 'tqdm','keras==2.2.4']
    )